# My Team Page Master

This README document provides an overview of the structure and design of your web page, which has been created using HTML, CSS, and the Flexbox layout.

###  Used content
* HTML
* CSS Styling
* Flexbox Layout


This web page is built using a combination of HTML and CSS, with the main layout structure implemented using Flexbox. The purpose of this page is to showcase your content in a visually appealing and flexible manner.

### Folder Structure
The folder structure for this project is as follows:
- images/
- index.html
- styles.css

The `index.html` file contains the HTML structure of your web page, and the `styles.css` file contains the CSS styling rules to customize the appearance.

### Render Link
https://my-team-page-maker.onrender.com